%%HP: T(0)A(D)F(.);
@ You may edit the T(0)A(D)F(.) parts.
@ The earlier parts of the line are used by Debug4x.

�

'DATA' PGDIR
'DATA' CRDIR

�
  "Cisalhamento: Solda de Filete" MSGBOX
  "Eletrodo"
  {
    { "60"  2 }
    { "70"  3 }
    { "80"  4 }
    { "90"  5 }
    { "100" 6 }
    { "110" 7 }
    { "120" 8 }
  }
  IF 1 CHOOSE NOT THEN KILL END
  { 'DATA' 'T9.6' } GETVAR
  2 GET SWAP GET
  1_MPa *
  "�adm" �TAG
�
'�Filete' STO

�
  "Tensao Primaria"
  {
    {"F" "Forca normal"}
    {"t ou h" "Alt. solda ou esp. chapa em mm"}
    {"l" "Comprimento em mm" }
  } { } { } { }
  IF INFORM NOT THEN KILL END
  OBJ� DROP * /
  1_MPa * "�1" �TAG
�
'�1' STO

�
  "Tensao Secundaria"
  {
    {"M" "Momento em N.mm"}
    {"r" "Distancia ate o centroide em mm"}
    {"I" "Momento de inercia em mm^4" }
  } { } { } { }
  IF INFORM NOT THEN KILL END
  OBJ� DROP / *
  1_MPa * "�2" �TAG
�
'�2' STO

�
  '�1' EVAL 2 ^
  '�2' EVAL 2 ^ +
  0.5 ^
  "�" �TAG
�
'�' STO

�
  "Tipo de carregamento"
  {
    {"Tracao" 0.60}
    {"Suporte" 0.90}
    {"Flexao" 0.60}
    {"Compressao Simples" 0.60}
    {"Cisalhamento" 0.30}
    {"Cis - Metal de base" 0.40}
  }
  IF 1 CHOOSE NOT THEN KILL END

  � Carregamento
  �
    "Tensao"
    {
      { "Especificar" 0 }
      { "E60xx"  1 }
      { "E70xx"   2 }
      { "E80xx"   3 }
      { "E90xx"   4 }
      { "E100xx" 5 }
      { "E120xx" 6 }
    }
    IF 1 CHOOSE NOT THEN KILL END EVAL

    IF DUP 0 == THEN
      DROP
      "Tabela 9-4"
      IF Carregamento 0.30 == THEN
        { { "Sut" "Tensao ultima de tracao em MPa" } }
      ELSE
        { { "Sy" "Tensao de escoamento em MPa" } }
      END
      { } { } { }
      IF INFORM NOT THEN KILL END
    ELSE
      { 'DATA' 'T9.3' } GETVAR
      SWAP GET
      IF Carregamento 0.30 == THEN 2 ELSE 3 END
      GET
    END
    
    Carregamento * 1_MPa * "Sadm" �TAG
  �
�
'Sadm' STO

@Tabela 9-3
{
  {60 427 345}
  {70 482 393}
  {80 551 462}
  {90 620 531}
  {100 689 600}
  {120 827 737}
}
{ 'DATA' 'T9.3' } SETVAR

{
  { 0  60   70 80 90 100 110 120 }
  { 0  124  145 165 186 207 228 248 }
}
{ 'DATA' 'T9.6' } SETVAR

"SOLDATEUR: Instalado" MSGBOX
�
