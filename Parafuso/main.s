%%HP: T(0)A(D)F(.);
@ You may edit the T(0)A(D)F(.) parts.
@ The earlier parts of the line are used by Debug4x.

�
@Cria Diretorios

'DATA' PGDIR
'DATA' CRDIR
'RESULT'PGDIR
'RESULT' CRDIR

@------------ PROGRAMAS -------------------------

�
IF 105 NEG FS? 1 == THEN
105 NEG CF
END

  "Sistema de Unidades"
  {
   { "SI"     1 }
   { "Ingles" 0 }
  }
  1 IF CHOOSE NOT THEN KILL END

  "Tipo de Agarre"
  {
   { "Com Porca" 1 }
   { "Roscado"   0 }
  }
  1 IF CHOOSE NOT THEN DROP KILL END

  "Tipo de Passo"
  {
   { "Fino"       3 }
   { "Grosso"     0 }
  }
  1 IF CHOOSE NOT THEN DROP DROP KILL END

  � SI Porca PassoFino
@---------Subprograma--------------@
  �
   DATA
@-----------Escolhe Parafuso------------@
      "   Escolher Parafuso "
      IF SI 1 == THEN
      T8.1
      ELSE
      T8.2
      END
      OBJ� � Linhas
      �
       3 Linhas FOR Cont1
          1 GET �STR StripQ  Linhas Cont1 1 - - 2 �LIST
      Linhas ROLLD
      NEXT
     Linhas 2 -
   �
          SWAP DROP SWAP DROP �LIST  1
          IF CHOOSE NOT THEN  UPDIR KILL
     END

     � Escolha
     �
@-------------------Retira Dados do Parafuso da Tabela------------------@
      IF SI 1 == THEN
       T8.1
       Escolha 2 + GET 4 NDUPN DROP
       1 GET  'd' STO
       2 PassoFino + GET  'p'  STO
       3 PassoFino + GET  'At' STO
       4 PassoFino + GET  'Ar' STO
      ELSE
       T8.2
       Escolha 2 + GET 4 NDUPN   DROP
       2 GET  'd' STO
       3 PassoFino + GET INV  'p' STO
       4 PassoFino + GET  'At' STO
       5 PassoFino + GET  'Ar' STO
      END
     �

@--------Menu de Classes Para Escolher-------@
       "Tipo da Classe"
   {
   { "Grau SAE"       0 }
   { "Designacao ASTM"     1 }
   { "Designacao Metrica"     2 }
   } 1
  IF CHOOSE NOT THEN
  UPDIR KILL
  END
   � TipoClasse �

@--------------Escolhe a Classe--------------------@

      "   Escolher Classe  "
          IF TipoClasse 0 == THEN
          T8.9
          END
          IF TipoClasse 1 == THEN
          T8.10
          END
          IF TipoClasse 2 == THEN
          T8.11
          END
          OBJ� � Linhas
          �
       3 Linhas FOR Cont1
            DUP  DUP 1 GET �STR StripQ "> " + SWAP 2 GET �STR
        StripQ + "|A|" + SWAP 3 GET �STR StripQ + Linhas
        Cont1 1 - - 2 �LIST Linhas ROLLD
            NEXT
         Linhas 2 -
          �
      SWAP DROP SWAP DROP �LIST  1
          IF CHOOSE NOT THEN
     UPDIR KILL
     END
      � ClasseEscolha �

@--------------------Retira os Dados de Classe da Tabela--------@
          IF TipoClasse 0 == THEN
          T8.9
          END
          IF TipoClasse 1 == THEN
          T8.10
          END
          IF TipoClasse 2 == THEN
          T8.11
          END
          DUP  2 GET
     4 GET
     SWAP
          ClasseEscolha 2 + GET DUP
          DUP
          4 GET 4 PICK * '�prova' STO
          5 GET 3 PICK * '�ut' STO
          6 GET * '�esc' STO
       �
       �
@--------------------Se Houver Porca Escolhe a Porca--------------------------@
      IF Porca 1 == THEN
         "   Escolher Porca  "
        IF SI 1 == THEN
            TA.29SI
           ELSE
            TA.29ING
           END
            OBJ� � Linhas
           �
          3 Linhas FOR Cont1
             1 GET �STR StripQ  Linhas Cont1 1 - - 2 �LIST
          Linhas ROLLD
             NEXT Linhas 2 -
           �
            SWAP DROP SWAP DROP �LIST  1
             IF CHOOSE NOT THEN

       UPDIR KILL END
       �  Porcaa
       �
@-----------Retira Dados da Porca da Tabela-------------@
           IF SI 1 == THEN
           TA.29SI
           Porcaa 2 + GET
           3 GET  'H' STO
       ELSE
           TA.29ING
           Porcaa 2 + GET
           3 GET  'H' STO
           END
    �
   END

@-------------------Escolher Resistencia a Enduran�a--------------------@

   "Resistencia a Enduran�a  "
       T8.17 OBJ� � Linhas

    �
   3 Linhas FOR Cont1
      DUP DUP 1 GET  "  " + SWAP  2 GET  + " A " + SWAP 3 GET
   + Linhas Cont1 1 - - 2 �LIST Linhas ROLLD
      NEXT Linhas 2 -
      �

      SWAP DROP SWAP DROP �LIST  1
      IF CHOOSE NOT THEN  UPDIR KILL END
      � SE �
@-------------Retira Resistencia a Enduranca da Tabela-------@
       T8.17
       SE 2 + GET
       4 GET OBJ�
       'Se' STO
       �

       IF 105 NEG FS? 0 == THEN
       105 NEG SF
       END
@-------------Insere Comprimento de Agarre Com ou Sem Porca-----------@

  IF Porca 1 == THEN
      "Agarre"
      "l" "Comprimento de Agarre "     IF SI THEN "mm"  ELSE "pol"  END + 0 3 �LIST 1 �LIST
      { } { } { }

      IF INFORM NOT THEN
   KILL
   END
      OBJ� DROP
      'l' STO
   ELSE
      "Agarre"
      "h" "Comprimento de Agarre Efetivo "     IF SI THEN "mm"  ELSE "pol"  END + 0 3 �LIST 1 �LIST
      "t2" "Espessura da Chapa Roscada " IF SI THEN "mm" ELSE "pol" END + 0 3 �LIST 1 �LIST +
      { } { } { }
      IF INFORM NOT THEN
   KILL
   END
      OBJ� DROP
      't2' STO
      'h' STO
          IF t2 d < THEN
          h t2 2 / + 'l' STO
          ELSE
          h d 2 / + 'l' STO
          END
          
  END
@----------------Calcula Menor Comprimento Possivel-----------@
      IF Porca 1 == THEN
      "L>"
      l H + p 2 * + EVAL DUP 3 ROLL SWAP �STR + IF SI THEN " mm"  ELSE " pol"  END +
      ELSE
      "L>"
      h d 1.5 * + EVAL DUP 3 ROLL SWAP  �STR + IF SI THEN " mm"  ELSE " pol"  END +
      END
      MSGBOX
@--------Insere Comprimento do Parafuso e Modulo de Young-----@
      � Comp
      �  "L>" Comp �STR + "|" + "Young" +
            "L" "Comprimento em "     IF SI THEN "mm"  ELSE "pol"  END + 0 3 �LIST 1 �LIST
            "E" "Modulo de Young em " IF SI THEN "GPa" ELSE "Mpsi" END + 0 3 �LIST 1 �LIST +
            { } Comp 0 2 �LIST DUP
      IF INFORM NOT THEN KILL END
         �
      OBJ� DROP  'E' STO
                 'L' STO
@--------Calcula LT Com a Equa��o Correta----------@
    IF SI 1 == THEN
       IF L 125 � d 48 � AND  THEN
       d 2 * 6 +  'LT' STO
       END
       IF L 125 > L 200 � AND THEN
       d 2 * 12 +  'LT' STO
       END
       IF L 200 >  THEN
       d 2 * 25 +   'LT' STO
       END
    ELSE
       IF L 6 �  THEN
       d 2 * 1 4 / +   'LT' STO
       END
       IF L 6 >  THEN
       d 2 * 1 2 / +   'LT' STO
       END
    END
@--------------Contas--------------@
  L LT - 'ld' STO
  d 2 ^ � * 4 / EVAL 'Ad' STO
  l ld - 'lt' STO
  Ad At E * * Ad lt * At ld * + /   EVAL
  'Kb' STO

@--------Troca os Resultados de Pasta------------@
  IF Porca 1 == THEN
  { Kb lt Ad ld LT L E l H Ar At p d �prova �ut Se �esc } DUP DUP
  EVAL 17 �LIST = SWAP PURGE UPDIR RESULT OBJ� 1 SWAP FOR Contt
  EQ� SWAP STO
  NEXT
  { 'h' 't2' } PURGE
  ELSE
  { Kb lt Ad ld LT L E l Ar At p d t2 h �prova �ut Se �esc} DUP DUP
  EVAL 18 �LIST = SWAP PURGE UPDIR RESULT OBJ� 1 SWAP FOR Contt
  EQ� SWAP STO
  NEXT
  { 'H' } PURGE
  END
  UPDIR
@----------------FIM-----------------@
  �
�

'Parafuso' STO


�
    "Material da Estrutura"
   {
   { "Aco"       0 }
   { "Aluminio"     1 }
   { "Cobre"     2 }
   { "Ferro Fundido Cinza" 3 }
   } 1
  IF CHOOSE NOT THEN
  UPDIR KILL
  END
  � Material
  �
  { DATA T8.8} GETVAR
  Material 3 + GET
  DUP 3 GET SWAP DUP 4 GET SWAP 5 GET
  � Me auxA auxB
  �
   "Dados da Estrutura"
      "l" "Comprimento de Agarre  "   2 �LIST 1 �LIST
      "d" "Diametro Parafuso  "  2 �LIST 1 �LIST +
      "E" "Modulo de Elasticidade "  2 �LIST 1 �LIST +
      "A" "Coeficiente Adimensional "  2 �LIST 1 �LIST +
      "B" "Coeficiente Adimensional"  2 �LIST 1 �LIST +
      {2 } NOVAL NOVAL Me 1_GPa * auxA auxB  5 �LIST  DUP
      IF INFORM NOT THEN
   KILL
   END
   OBJ� DROP
   � ll dd EE AA BB
   � EE dd * AA * BB dd * ll / e SWAP ^  * EVAL km SWAP =
   �
  �
  �
�
'Estrutura' STO

@-------------- TABELAS --------------------------

@Tabela 8-17
{
  {"Grau ou Classe" "Int. de Tamanho Inicio" "Int. de Tamanho Final" "Resistencia Enduranca"}
  { 1 1 1 1 }
  {"SAE 5"    "1/4"    "1"     "18.6_Kpsi"}
  {"SAE 5"    "1+1/8"  "1+1/2" "16.3_Kpsi"}
  {"SAE 7"    "1/4"    "1+1/2" "20.6_Kpsi"}
  {"SAE 8"    "1/4"    "1+1/2" "23.2_Kpsi"}
  {"ISO 8.8"  "M16"       "M36"      "129_MPa"}
  {"ISO 9.8"  "M1.6"      "M16"      "140_MPa"}
  {"ISO 10.9" "M5"        "M36"      "162_MPa"}
  {"ISO 12.9" "M1.6"      "M36"      "192_MPa"}
}
{ 'DATA' 'T8.17' } SETVAR


@Tabela A-29 (SI)
{
  {"Medida Nominal" "Largura W" "Hexagonal Regular" "Espesaou de fenda" "Travamento"}
  { 1 '1_mm' '1_mm' '1_mm' '1_mm' }
  {"M5"  8  4.7  5.1  2.7}
  {"M6"  10 5.2  5.7  3.2}
  {"M8"  13 6.8  7.5  4.0}
  {"M10" 16 8.4  9.3  5.0}
  {"M12" 18 10.8 12.0 6.0}
  {"M14" 21 12.8 14.1 7.0}
  {"M16" 24 14.8 16.4 8.0}
  {"M20" 30 18.0 20.3 10.0}
  {"M24" 36 21.5 23.9 12.0}
  {"M30" 46 25.6 28.6 15.0}
  {"M36" 55 31.0 34.7 18.0}
}
{ 'DATA' 'TA.29SI' } SETVAR

@Tabela A-29 (Ingles)
{
  {"Medida Nominal" "Largura W" "Hexagonal Regular" "Espesaou de fenda" "Travamento"}
  { '1_in' '1_in' '1_in' '1_in' '1_in' }
  {'1/4'   '7/16'    '7/32'    '9/32'   '5/32'}
  {'5/16'  '1/2'     '17/64'   '21/64'  '3/16'}
  {'3/8'   '9/16'    '21/64'   '13/32'  '7/32'}
  {'7/16'  '11/16'   '3/8'     '29/64'  '1/4'}
  {'1/2'   '3/4'     '7/16'    '9/16'   '5/16'}
  {'9/16'  '7/8'     '31/64'   '39/64'  '5/16'}
  {'5/8'   '15/16'   '35/64'   '23/32'  '3/8'}
  {'3/4'   '1+1/8'   '41/64'   '13/16'  '27/64'}
  {'7/8'   '1+5/16'  '3/4'     '29/32'  '31/64'}
  {'1'     '1+1/2'   '55/64'   '1'      '35/64'}
  {'1+1/8' '1+11/16' '31/32'   '1+5/32' '39/64'}
  {'2+1/4' '1+7/8'   '1+1/16'  '1+1/4'  '23/32'}
  {'1+3/8' '2+1/16'  '1+11/64' '1+3/8'  '25/32'}
  {'1+1/2' '2+1/4'   '1+9/32'  '1+1/2'  '27/32'}
}
{ 'DATA' 'TA.29ING' } SETVAR

@Tabela 8-11
{
  {"Categoria de Propriedade" "Int. de tamanho inclusivo Inicio" "Int. de tamanho inclusivo Fim" "Resistencia minima de Prova" "Resistencia Minima de tracao" "Resistencia minima de escoamento"}
  {1 1 1 1_MPa 1_MPa 1_MPa}
  {4.6  "M5"   "M36" 225 400  240}
  {4.8  "M1.6" "M16" 310 420  340}
  {5.8  "M5"   "M24" 380 520  420}
  {8.8  "M16"  "M36" 600 830  660}
  {9.8  "M1.6" "M16" 650 900  720}
  {10.9 "M5"   "M36" 830 1040 940}
  {12.9 "M1.6" "M36" 970 1220 1100}
}
{ 'DATA' 'T8.11' } SETVAR

@Tabela 8-9
{
  {"Grau SAE" "Int. de tamanho inclusivo INICIO" "Int. de tamanho inclusivo FIM" "Resistencia minima de prova" "Resistencia minima de tracao" "Resistencia minima de escoamento"}
  {1 '1_in' '1_in' '1_Kpsi' '1_Kpsi' '1_Kpsi'}
  {1   '1/4'   '1+1/2' 33  60  36}
  {2   '1/4'   '3/4'   55  74  57}
  {2   '7/8'   '1+1/2' 33  60  36}
  {4   '1/4'   '1+1/2' 65  115 100}
  {5   '1/4'   '1'     85  120 92}
  {5   '1+1/8' '1+1/2' 74  105 81}
  {5.2 '1/4'   '1'     85  120 92}
  {7   '1/4'   '1+1/2' 105 133 115}
  {8   '1/4'   '1+1/2' 120 150 130}
  {8.2 '1/4'   '1'     120 150 130}
}
{ 'DATA' 'T8.9' } SETVAR

@Tabela 8-10
{
  {"Designacao ASTM" "Int. Tamanho Inclusivo Inicio" "Int. Tamanho Inclusivo Fim" "Resistencia minima de prova" "Resistencia minima de tracao" "Resistencia minima de escoamento" "Material"}
  {1 '1_in' '1_in' '1_Kpsi' '1_Kpsi' '1_Kpsi' 1}
  {"A307"    '1/4'   '1+1/2' 33  60  36  "Baixo carbono"}
  {"A325-1"  '1/2'   '1'     85  120 92  "Medio carbono, QeT"}
  {"A325-1"  '1+1/8' '1+1/2' 74  105 81  "Medio carbono, QeT"}
  {"A325-2"  '1/2'   '1'     85  120 92  "Medio carbono, Martensita, QeT"}
  {"A325-2"  '1+1/8' '1+1/2' 74  105 81  "Medio carbono, Martensita, QeT"}
  {"A325-3"  '1/2'   '1'     85  120 92  "Aco envelhecido QeT"}
  {"A325-3"  '1+1/8' '1+1/2' 74  105 81  "Aco envelhecido QeT"}
  {"A354-BC" '1/4'   '2+1/2' 105 125 109 "Aco-Liga QeT"}
  {"A354-BC" '2+3/4' '4'     95  115 99  "Aco-Liga QeT"}
  {"A354-BD" '1/4'   '4'     120 150 130 "Aco-Liga QeT"}
  {"A449"    '1/4'   '1'     85  120 92  "Medio carbono QeT"}
  {"A449"    '1+1/8' '1+1/2' 74  105 81  "Medio carbono QeT"}
  {"A449"    '1+3/4' '3'     55  90  58  "Medio carbono QeT"}
  {"A490-1"  '1/2'   '1+1/2' 120 150 130 "Aco-Liga QeT"}
  {"A490-3"  '1/2'   '1+1/2' 120 150 130 "Aco envelhecido QeT"}
}
{ 'DATA' 'T8.10' } SETVAR

@Tabela 8-1
{
  { "Diametro maior nominal d" "P.Grosso-Passo p" "P.Grosso-Area de tensao de tracao At" "P.Grosso-Area de diametro menor Ar" "P.Fino-Passo p" "P.Fino-Area de tensao de tracao At" "P.Fino-Area de diametro menor Ar"}
  { '1_mm' '1_mm' '1_mm^2' '1_mm^2' '1_mm' '1_mm^2' '1_mm^2'}
  { 1.6  0.35  1.27  1.7   0     0     0   }
  { 2    0.40  2.07  1.79  0     0     0   }
  { 2.5  0.45  3.39  2.98  0     0     0   }
  { 3    0.5   5.03  4.47  0     0     0   }
  { 3.5  0.6   6.78  6.00  0     0     0   }
  { 4    0.7   8.78  7.75  0     0     0   }
  { 5    0.8   14.2  12.7  0     0     0   }
  { 6    1     20.1  17.9  0     0     0   }
  { 8    1.25  36.6  32.8  1     39.2  36.0}
  { 10   1.5   58.0  52.3  1.25  61.2  56.3}
  { 12   1.75  84.3  76.3  1.25  92.1  86.0}
  { 14   2     114   104   1.5   125   116 }
  { 16   2     157   144   1.5   167   157 }
  { 20   2.5   245   225   1.5   272   259 }
  { 24   3     353   324   2     384   365 }
  { 30   3.5   561   519   2     621   596 }
  { 36   4     817   759   2     915   884 }
  { 42   4.5   1120  1050  2     1260  1230}
  { 48   5     1470  1380  2     1670  1630}
  { 56   5.5   2030  1910  2     2300  2250}
  { 64   6     2680  2520  2     3030  2980}
  { 72   6     3460  3280  2     3860  3800}
  { 80   6     4340  4140  1.5   4850  4800}
  { 90   6     5590  5360  2     6100  6020}
  { 100  6     6990  6740  2     7560  7470}
  { 110  0     0     0     2     9180  9080}
}
{ 'DATA' 'T8.1' } SETVAR


@Tabela 8-8
{
  { "Material usado" "Razao de Poisson" "Modulo Elasticos" "A" "B" }
  { 1 1 1 1 1 }
  {"A�o" 0.291 207 0.78715 0.62873}
  {"Aluminio" 0.334 71 0.79670 0.63816}
  {"Cobre" 0.326 119 0.79568 0.63553}
  {"Ferro fundido cinza" 0.211 100 0.77871 0.61616}
  {"Expressao Geral" 0 0 0.78952 0.62914}
}
{ 'DATA' 'T8.8' } SETVAR

@Tabela 8-2
{
  {"Designacao de tamanho" "Diametro maior nominal" "Grossa-Roscar por polegada N" "Grossa-Area de tensao At de tracao" "Grossa-Area de diametro menor Ar" "Fina-Roscas por polegada N" "Fina-Area de tensao de tracao At" "Fina-Area de diametro menor Ar"}
  {1 '1_in' 1 '1_in^2' '1_in^2' 1 '1_in^2' '1_in^2' }
  { 0       0.600  0  0       0       80  0.00180  0.00151}
  { 1       0.0730 64 0.00263 0.00218 72  0.00278  0.00237}
  { 2       0.0860 56 0.00370 0.00310 64  0.00394  0.00339}
  { 3       0.0990 48 0.00487 0.00406 56  0.00523  0.00451}
  { 4       0.1120 40 0.00604 0.00496 48  0.00661  0.00566}
  { 5       0.1250 40 0.00796 0.00672 44  0.00880  0.00716}
  { 6       0.1380 32 0.00909 0.00745 40  0.01015  0.00874}
  { 8       0.1640 32 0.0140  0.01196 36  0.01474  0.01285}
  { 10      0.1900 24 0.0175  0.01450 32  0.0200   0.0175 }
  { 12      0.2160 24 0.0242  0.0206  28  0.0258   0.0226 }
  { '1/4'   0.2500 20 0.0318  0.0269  28  0.0364   0.0326 }
  { '5/16'  0.3125 18 0.0524  0.0454  24  0.0580   0.0524 }
  { '3/8'   0.3750 16 0.0775  0.0678  24  0.0878   0.0809 }
  { '7/16'  0.4375 14 0.1063  0.0933  20  0.1187   0.1090 }
  { '1/2'   0.500 13 0.1419  0.1257  20  0.1599   0.1486 }
  { '9/16'  0.5625 12 0.182   0.162   18  0.203    0.189  }
  { '5/8'   0.6250 11 0.226   0.202   18  0.256    0.240  }
  { '3/4'   0.7500 10 0.334   0.302   16  0.373    0.351  }
  { '7/8'   0.8750 9  0.462   0.419   14  0.509    0.480  }
  { '1'     1.0000 8  0.606   0.551   12  0.663    0.625  }
  { '1+1/4' 1.2500 7  0.969   0.890   12  1.073    1.024  }
  { '1+1/2' 1.5000 6  1.405   1.294   12  1.581    1.521  }
}
{ 'DATA' 'T8.2' } SETVAR
DATA { T8.1 T8.2 T8.8 T8.9 T8.10 T8.11 T8.17 TA.29SI TA.29ING }  ORDER
UPDIR

� � STR
  �
    IF STR "'" POS 0. �
    THEN STR 2. STR SIZE 1. - SUB
    ELSE STR
    END
  �
�
'StripQ' STO

"PARAFINATOR: Instalado" MSGBOX
"by Doug" MSGBOX
�
