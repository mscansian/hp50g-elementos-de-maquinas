%%HP: T(0)A(D)F(.);
@You may edit the T(0)A(D)F(.) parts.
@The earlier parts of the line are used by Debug4x.


@Instalador
�
@Cria Diret�rios
'DATA' PGDIR
'DATA' CRDIR

@Programa Principal
�
1 GETQUERY
"Reducao"
� Propriedade
�
@Cria Matriz de Nomes
'DATA' 'MatMatrix' 2 �LIST GETVAR
1 'DATA' 'StripCOL' 2 �LIST GETVAR

@Cria Matriz de Criterios
'DATA' 'MatMatrix' 2 �LIST GETVAR
1 'DATA' 'StripROW' 2 �LIST GETVAR

@Calcular tamanho da matriz
'DATA' 'MatMatrix' 2 �LIST GETVAR SIZE OBJ� DROP

� TempNameMatrix TempCritMatrix TotalL TotalC
�
@Propriedade
"Escolha a Propriedade"
IF Propriedade NOVAL � THEN
  4 TotalC FOR Num
    @Joga a matriz na pilha
    TempCritMatrix
    @Pega valor da matriz
    Num 1. �LIST GET �STR
    @Retira primeiro e ultimo caractere (aspas simples)
    TAIL DUP SIZE 1 - 1 SWAP SUB DUP
    @Verifica se nome � igual
    IF Propriedade == THEN
      @Cria uma lista
      Num 2 �LIST 1 �LIST
      "PROPRIEDADE_OK"
    ELSE
      DROP
    END
  NEXT
END
DUP IF "PROPRIEDADE_OK" � THEN
  { }
  2 TotalC FOR Num
    @Joga a matriz na pilha
    TempCritMatrix
    @Pega valor da matriz
    Num 1. �LIST GET �STR
    @Retira primeiro e ultimo caractere (aspas simples)
    TAIL DUP SIZE 1 - 1 SWAP SUB
    @Joga numero da coluna e transforma em lista
    Num 2 �LIST 1 �LIST +
  NEXT
ELSE
  DROP
END
  1

  IF CHOOSE THEN
    @Acha Unidade
    DUP 'DATA' 'MatMatrix' 2 �LIST GETVAR SWAP
    2 SWAP 2. �LIST GET �STR
    � Coluna Unidade
    �
      @Cria Matriz de Valores
      'DATA' 'MatMatrix' 2 �LIST GETVAR
      Coluna 'DATA' 'StripCOL' 2 �LIST GETVAR

      � TempValMatrix
      �
       "Escolha o Material"
       @Cria matriz em branco
       { }
       @Nome do A�o - Valor do Criterio
       3 TotalL FOR Num
         @Joga a matriz na pilha
         TempNameMatrix
         @Pega valor da matriz (Nome)
         Num 1. �LIST GET �STR
         @Retira primeiro e ultimo caractere (aspas simples)
         TAIL DUP SIZE 1 - 1 SWAP SUB DUP
         @Pega valor da matriz (Criterio)
         TempValMatrix
         Num 1. �LIST GET DUP 3 ROLLD �STR


         @Monta String
         ":" SWAP + +

         @Coloca Unidade
         Unidade DUP
         IF "0" == THEN
           DROP 3 ROLLD SWAP �TAG
         ELSE
           @Retira primeiro e ultimo caractere (aspas simples)
           TAIL DUP SIZE 1 - 1 SWAP SUB DUP
           " [" SWAP + "]" + SWAP 3 ROLLD +

          
           @Monta String
           3 ROLLD OBJ� _ 3 ROLL �TAG
         END


         @Joga o valor da Linha para o Choose e cria uma lista
         2 �LIST
         @Cria mais uma lista e soma
         1 �LIST +
       NEXT
      1
      IF CHOOSE THEN
      END
    � �

END
�
�
�
'Dados' STO

@Seleciona Coluna de uma matriz
�
  � Matriz Coluna
  �
    Matriz �COL DUP
    Coluna - 2 + PICK
    � Valores
    �
      DROPN Valores
    �
  �
�
'DATA' 'StripCOL' 2 �LIST SETVAR

@Seleciona Linha de uma matriz
�
  � Matriz Linha
  �
    Matriz �ROW DUP
    Linha - 2 + PICK
    � Valores
    �
      DROPN Valores
    �
  �
�
'DATA' 'StripROW' 2 �LIST SETVAR

@Cria matriz dos materiais
[[ 'Classe' 'DurezaHB' '�utMpa' '�utKsi' 'Reducao' 'Deformacao' 'EGpa' 'EMpsi' '�fMpa' '�fksi' 'ExpFadiga' 'CoefDucti' 'ExpDucti' ]
 [ '0' '0' 'MPa' 'kpsi' '0' '0' 'GPa' 'Gpsi' 'MPa' 'kpsi' '0' '0' '0' ]
 [ 'A538A�L�STA' 405. 1515. 220. 67. 1.1 185. 27. 1655. 240. -.065 .3 -.62 ]
 [ 'A538B�L�STA' 460. 1860. 270. 56. .82 185. 27. 2135. 310. -.071 .8 -.71 ]
 [ 'A538C�L�STA' 480. 2000. 290. 55. .81 180. 26. 2240. 325. -.07 .6 -.75 ]
 [ 'AM350�L�HReA' 480. 1315. 191. 52. .74 195. 28. 2800. 406. -.14 .33 -.84 ]
 [ 'AM350�L�CD' 496. 1905. 276. 20. .23 180. 26. 2690. 390. -.102 .1 -.42 ]
 [ 'Gainex�LT�HReLam' 496. 530. 77. 58. .86 200. 29.2 805. 117. -.07 .86 -.65 ]
 [ 'Gainex�L�HReLamo' 496. 510. 74. 64. 1.02 200. 29.2 805. 117. -.071 .86 -.68 ]
 [ 'H11�L�ConfAust' 660. 2585. 375. 33. .4 205. 30. 3170. 460. -.077 .08 -.74 ]
 [ 'RQC100�LT�HRPlaca' 290. 940. 136. 43. .56 205. 30. 1240. 180. -.07 .66 -.69 ]
 [ 'RQC100�L�HRPlaca' 290. 930. 135. 67. 1.02 205. 30. 1240. 180. -.07 .66 -.69 ]
 [ 'A10B62�L�Q&T' 430. 1640. 238. 38. .89 195. 28. 1780. 258. -.067 .32 -.56 ]
 [ 'A1005A9�LT�HRLam' 90. 360. 52. 73. 1.3 205. 30. 580. 84. -.09 .15 -.43 ]
 [ 'A1005A9�LT�CDLam' 125. 470. 68. 66. 1.09 205. 30. 515. 75. -.059 .3 -.51 ]
 [ 'A1005A9�L�CDLam' 125. 415. 60. 64. 1.02 200. 29. 540. 78. -.073 .11 -.41 ]
 [ 'A1005A9�L�HRLam' 90. 345. 50. 80. 1.6 200. 29. 640. 93. -.109 .1 -.39 ]
 [ 'A1015�L�Norm' 80. 415. 60. 68. 1.14 205. 30. 825. 120. -.11 .95 -.64 ]
 [ 'A1020�L�HRPlaca' 108. 440. 64. 62. .96 205. 29.5 895. 130. -.12 .41 -.51 ]
 [ 'A1040�L�Forj' 225. 620. 90. 60. .93 200. 29. 1540. 223. -.14 .61 -.57 ]
 [ 'A1045�L�Q&T' 225. 725. 105. 65. 1.04 200. 29. 1225. 178. -.095 1. -.66 ]
 [ 'A1045�L�Q&T' 410. 1450. 210. 51. .72 200. 29. 1860. 270. -.073 .6 -.7 ]
 [ 'A1045�L�Q&T' 390. 1345. 195. 59. .89 205. 30. 1585. 230. -.074 .45 -.68 ]
 [ 'A1045�L�Q&T' 450. 1585. 230. 55. .81 205. 30. 1795. 260. -.07 .35 -.69 ]
 [ 'A1045�L�Q&T' 500. 1825. 265. 51. .71 205. 30. 2275. 330. -.08 .25 -.68 ]
 [ 'A1045�L�Q&T' 595. 2240. 325. 41. .52 205. 30. 2725. 395. -.08 10.07 -.6 ]
 [ 'A1144�L�CDSR' 265. 930. 135. 33. .51 195. 28.5 1000. 145. -.08 .32 -.58 ]
 [ 'A1144�L�DAT' 305. 1035. 150. 25. .29 200. 28.8 1585. 230. -.09 .27 -.53 ]
 [ 'A1541F�L�Q&TForj' 290. 950. 138. 49. .68 205. 29.9 1275. 185. -.076 .68 -.65 ]
 [ 'A1541F�L�Q&TForj' 260. 890. 129. 60. .93 205. 29.9 1275. 185. -.071 .93 -.65 ]
 [ 'A4130�L�Q&T' 258. 895. 130. 67. 1.12 220. 32. 1275. 185. -.083 .92 -.63 ]
 [ 'A4130�L�Q&T' 365. 1425. 207. 55. .79 200. 29. 1695. 246. -.081 .89 -.69 ]
 [ 'A4140�L�Q&TeDAT' 310. 1075. 156. 60. .69 200. 29.2 1825. 265. -.08 1.2 -.59 ]
 [ 'A4142�L�DAT' 310. 1060. 154. 29. .35 200. 29. 1450. 210. -.1 .22 -.51 ]
 [ 'A4142�L�DAT' 335. 1250. 181. 28. .34 200. 28.9 1250. 181. -.08 .06 -.62 ]
 [ 'A4142�L�Q&T' 380. 1415. 205. 48. .66 205. 30. 1825. 265. -.08 .45 -.75 ]
 [ 'A4142�L�Q&TDef' 400. 1550. 225. 47. .63 200. 29. 1895. 275. -.09 .5 -.75 ]
 [ 'A4142�L�Q&T' 450. 1760. 255. 42. .54 205. 30. 2000. 290. -.08 .4 -.73 ]
 [ 'A4142�L�Q&TDef' 475. 2035. 295. 20. .22 200. 29. 2070. 300. -.082 .2 -.77 ]
 [ 'A4142�L�Q&TDef' 450. 1930. 280. 37. .46 200. 29. 2105. 305. -.09 .6 -.76 ]
 [ 'A4142�L�Q&T' 475. 1930. 280. 35. .43 205. 30. 2170. 315. -.081 .09 -.61 ]
 [ 'A4142�L�Q&T' 560. 2240. 325. 27. .31 205. 30. 2655. 385. -.089 .07 -.76 ]
 [ 'A4340�L�HReA' 243. 825. 120. 43. .57 195. 28. 1200. 174. -.095 .45 -.54 ]
 [ 'A4340�L�Q&T' 409. 1470. 213. 38. .48 200. 29. 2000. 290. -.091 .48 -.6 ]
 [ 'A4340�L�Q&T' 350. 1240. 180. 57. .84 195. 28. 1655. 240. -.076 .73 -.62 ]
 [ 'A5160�L�Q&T' 430. 1670. 242. 42. .87 195. 28. 1930. 280. -.071 .4 -.57 ]
 [ 'A52100�L�SHQ&T' 518. 2015. 292. 11. .12 205. 30. 2585. 375. -.09 .18 -.56 ]
 [ 'A9262�L�A' 260. 925. 134. 14. .16 205. 30. 1040. 151. -.07 10.16 -.47 ]
 [ 'A9262�L�Q&T' 280. 1000. 145. 33. .41 195. 28. 1220. 177. -.07 30.41 -.6 ]
 [ 'A9262�L�Q&T' 410. 565. 227. 32. .38 200. 29. 1855. 269. -.057 .38 -.65 ]
 [ 'A950C�LT�HRPlaca' 159. 565. 82. 64. 1.03 205. 29.6 1170. 170. -.12 .95 -.61 ]
 [ 'A950C�L�HRBarra' 150. 565. 82. 69. 1.19 205. 30. 970. 141. -.11 .85 -.59 ]
 [ 'A950X�L�PlacaCanal' 150. 440. 64. 65. 1.06 205. 30. 625. 91. -.075 .35 -.54 ]
 [ 'A950X�L�HRPlaca' 156. 530. 77. 72. 1.24 205. 29.5 1005. 146. -.1 .85 -.61 ]
 [ 'A950X�L�PlacaCanal' 225. 695. 101. 68. 1.15 195. 28.2 1055. 153. -.08 .21 -.53 ]]
 'DATA' 'MatMatrix' 2 �LIST SETVAR

CLLCD "Materiais: Instalado" MSGBOX
�
