%%HP: T(0)A(D)F(.);
@ You may edit the T(0)A(D)F(.) parts.
@ The earlier parts of the line are used by Debug4x.

�

@Cria diretorio
'DATA' PGDIR
'DATA' CRDIR

@Cria arquivos necessarios
[ 1 ] 'DATA' 'MyCond' 2 �LIST SETVAR

�
  1 GETQUERY
  
  IF NOVAL == THEN
    "Usar condicoes anteriores?"
    { { "Sim" 1 } { "Nao" NOVAL } }
    IF 2 CHOOSE NOT THEN KILL END
  ELSE
    1
  END

  @Cria matriz de condicoes com base na matriz padrao
  IF NOVAL == THEN
    'DATA' 'DefaultCond' 2 �LIST GETVAR
    'DATA' 'MyCond' 2 �LIST SETVAR
  END

  @Cria matrizes
  { } 'DATA' 'MyIq' 2 �LIST SETVAR
  { } 'DATA' 'MyEq' 2 �LIST SETVAR
  { } 'DATA' 'MyVars' 2 �LIST SETVAR
  { } 'DATA' 'MyIteracoes' 2 �LIST SETVAR
  { } 'DATA' 'MyIteracoesV' 2 �LIST SETVAR


  @Pega matriz de condicoes
  'DATA' 'MyCond' 2 �LIST GETVAR

  DO
    @Duplica matriz
    DUP
    
    @Salva o tamanho da matriz em uma variavel Local
    SIZE OBJ� DROP
    � MyCondicao MyCondSize
    �
      "Condicoes"
      {
       { "Continuar" 0 }
      }

      1 MyCondSize FOR Cont
        @Pega valor da matriz pelo Cont
        MyCondicao Cont 1 �LIST GET DUP

        IF 1 SAME NOT THEN
          @Cria item no menu do choose
          Cont 2 �LIST 1 �LIST +
        ELSE
          DROP
        END
      NEXT


      {
       { "Nova Condicao" -1}
      } +


      IF 1 CHOOSE NOT THEN
        @Finaliza o programa
        KILL
      ELSE
        @Salva Opcao do CHOOSE dentro de uma LOCAL VAR
        � Option
        �
          CASE
            Option 0 == THEN
              1 @Sair do LOOP
            END

            Option -1 == THEN
              "Digite a nova condicao"
              "'"
              INPUT OBJ�
              MyCondicao OBJ� OBJ� DROP �LIST @Pega a matriz e transofrma em lista
              SWAP 1 �LIST + @Adiciona elemento no final da lista
              OBJ� �ARRY DUP @Transforma em matriz
              'DATA' 'MyCond' 2 �LIST SETVAR @Salva Matriz
              0 @Continua no LOOP
            END

            @Caso selecione alguma condicao
            "Editar condicao"
            MyCondicao Option 1 �LIST GET �STR
            INPUT OBJ�
            IF DUP 0 SAME NOT THEN
              MyCondicao SWAP
              Option 1 �LIST SWAP PUT DUP
              'DATA' 'MyCond' 2 �LIST SETVAR @Salva Matriz
            END
            0 @Continua no LOOP
          END
        �
      END
    �
    CLLCD
  UNTIL
  END

  "      PREPARANDO" 5
  "       EQUACOES " 6
  CLLCD DISP DISP
  
  @Separador de equacoes e inequacoes
  'DATA' 'MyCond' 2 �LIST GETVAR
  DUP SIZE OBJ� DROP

  � MyCondicao MyCondSize
  �
    @Separa equacoes de inequacoes
    1 MyCondSize FOR Cont
      MyCondicao Cont 1 �LIST GET DUP DUP @Pega valor
      IF DUP 1 SAME NOT THEN
        EQ�
        IF 3 ROLLD SAME THEN
          @Inequacao
          DROP
          'DATA' 'MyIq' 2 �LIST GETVAR @Pega lista
          SWAP + @Adiciona elemento
          'DATA' 'MyIq' 2 �LIST SETVAR @Salva lista
        ELSE
          @Equacao
          DROP
          'DATA' 'MyEq' 2 �LIST GETVAR @Pega lista
          SWAP + @Adiciona elemento
          'DATA' 'MyEq' 2 �LIST SETVAR @Salva lista
        END
      ELSE
        3 DROPN
      END
    NEXT
  �

  @Verifica condicoes
  IF 'DATA' 'MyEq' 2 �LIST GETVAR SIZE 0 == THEN "Nao ha equacoes!"   MSGBOX KILL END
  IF 'DATA' 'MyIq' 2 �LIST GETVAR SIZE 0 == THEN "Nao ha inequacoes!" MSGBOX KILL END

  @Seleciona variavel para iteracao
  'DATA' 'MyEq' 2 �LIST GETVAR OBJ� �ARRY LNAME OBJ� EVAL
  � NumVars
  �
    1 NumVars START
      DUP 2 �LIST 1 �LIST
      NumVars ROLLD
    NEXT

    1 NumVars 1 - START
      +
    NEXT

    "Variavel para iterar"
    SWAP 1
    IF CHOOSE NOT THEN DROP KILL END

    @Deleta matriz com as variaveis
    SWAP DROP
    'DATA' 'MyVarName' 2 �LIST SETVAR @Salva nome da variavel
  �

  @Configuracoes de iteracao
  DO
    "Iteracoes"
    {
      { "Valor Inicial" "Valor inicial da variavel" }
      { "Valor Final" "Valor final da variavel" }
      { "Incremento" "Incremento" }
    }
    { } DUP DUP

    IF INFORM NOT THEN
      KILL
    END

    @Salva dados
    OBJ� DROP 3 DUPN
    'DATA' 'MyIncremento' 2 �LIST SETVAR
    'DATA' 'MyFinal' 2 �LIST SETVAR
    'DATA' 'MyValue' 2 �LIST SETVAR

    � MyValue MyFinal MyIncremento
    �
      @Verifica se dados estao consistentes
      CASE
        MyValue MyFinal == THEN
          "Valor Inicial eh igual ao final - Nao iterativo" MSGBOX 1
        END
        MyValue MyFinal < MyIncremento 0 > XOR THEN
          "Incremento nao converge" MSGBOX 0
        END
        1
      END
    �
  UNTIL
  END


  @Cria variavel de iteracao
  'DATA' 'MyVarName' 2 �LIST GETVAR
  � MyVarName
  �
    MyVarName 1 �LIST
    'DATA' 'MyVars' 2 �LIST GETVAR +
    'DATA' 'MyVars' 2 �LIST SETVAR @Salva nome da variavel
  
    @Inicia iteracoes
    DO
      @Limpa variaveis
      'DATA' 'MyVars' 2 �LIST GETVAR PURGE

      
      @Cria variavel de iteracao
      'DATA' 'MyValue' 2 �LIST GETVAR
      MyVarName
      STO

      @Mostra tela
      "      "
      MyVarName �STR
      " = "
      MyVarName EVAL
      + + + 6
      "     CALCULANDO..." 5
      CLLCD DISP DISP

      @Calcula equacoes
      NOVAL @Joga um valor em branco para nao dar erro na comparacao

      DO
        'DATA' 'MyEq' 2 �LIST GETVAR SIMPLIFY DUP 3 ROLLD @Calcula equacoes e organiza dados
        @DUP �STR MSGBOX
        IF SAME NOT THEN
          DUP SIZE
          � MyEq MyEqSize
          �
            1 MyEqSize FOR Cont
              MyEq Cont 1 �LIST GET SIMPLIFY @Pega valor

              @Verifica se o valor esta OK
              IF DUP ? SAME NOT THEN
                @Verifica se soh existe 1 icognita
                IF LNAME SIZE EVAL 1 == THEN
                  LNAME OBJ� DROP

                  @Confere se a equacao nao eh 1=0


                  @CORTEI FORA ESSA PARTE POIS ESTAVA BUGADA
                  @IF DUP EQ� SAME 0 == THEN
                  @  'DATA' 'MyVars' 2 �LIST GETVAR PURGE
                  @
                  @  2 DROPN
                  @
                  @  "Fisicamente Impossivel" MSGBOX
                  @  "Rodar o programa novamente?"
                  @  { { "Sim" 1 } { "Nao" 0 } }
                  @  IF 1 CHOOSE THEN
                  @    IF 1 == THEN 1 'MCilindrica' CALL END
                  @  END
                  @  KILL
                  @END
                  
                  ISOL @Pega variavel e isola
                  EQ� SWAP
                  DUP 1 �LIST
                  'DATA' 'MyVars' 2 �LIST GETVAR +
                  'DATA' 'MyVars' 2 �LIST SETVAR @Salva nome da variavel
                  STO @Salva variavel
                ELSE
                  IF LNAME DUP TYPE 5 � THEN
                    OBJ� EVAL 1 - DROPN
                    ISOL EQ� EVAL SWAP
                    DUP 1 �LIST
                    'DATA' 'MyVars' 2 �LIST GETVAR +
                    'DATA' 'MyVars' 2 �LIST SETVAR @Salva nome da variavel
                    STO
                  ELSE
                    2 DROPN
                  END
                END
              ELSE @Equacoes linearmente dependentes
                DROP

                'DATA' 'MyVars' 2 �LIST GETVAR PURGE @Limpa variaveis
                "ERRO: Impossivel calcular variaveis" MSGBOX
                "Ha mais variaveis que equacoes" MSGBOX

                "Rodar o programa novamente?"
                { { "Sim" 1 } { "Nao" 0 } }
                IF 1 CHOOSE THEN
                  IF 1 == THEN 1 'MCilindrica' CALL END
                END
                KILL
              END
            NEXT
            MyEq @Joga equacao para comparar na proxima iteracao
            0 @Repetir Loop
          �
        ELSE @Caso ele nao tenha mais o que calcular

          @Verifica se todas variaveis foram encontradas
          OBJ� �ARRY
          IF LNAME SIZE EVAL 0 � THEN
            'DATA' 'MyVars' 2 �LIST GETVAR PURGE @Limpa variaveis
            "ERRO: Ha equacoes LDs, com as variaveis: " MSGBOX
            LNAME �STR MSGBOX
            DROP
            'DATA' 'MyVars' 2 �LIST GETVAR PURGE @Limpa variaveis
            "Rodar o programa novamente?"
            { { "Sim" 1 } { "Nao" 0 } }
            IF 1 CHOOSE THEN
              IF 1 == THEN 1 'MCilindrica' CALL END
            END
            KILL
          END

          DROP
          1 @Sair do Loop
        END
      UNTIL
      END

      @Salva Valor de variaveis
      'DATA' 'MyEq' 2 �LIST GETVAR OBJ� �ARRY LNAME SWAP DROP SIZE OBJ� DROP
      � NumVars
      �
        1 NumVars FOR Cont
          'DATA' 'MyEq' 2 �LIST GETVAR OBJ� �ARRY LNAME SWAP DROP
          Cont 1 �LIST GET SIMPLIFY
        NEXT

        NumVars �LIST 1 �LIST
        'DATA' 'MyIteracoesV' 2 �LIST GETVAR
        SWAP +
        'DATA' 'MyIteracoesV' 2 �LIST SETVAR
      �

      @Calcula inequacoes
      'DATA' 'MyIq' 2 �LIST GETVAR
      DUP SIZE
      � MyIq MyIqSize
      �
        @Verifica se o programa tem todas as variaveis para as inequacoes
        MyIq SIMPLIFY OBJ� �ARRY
        IF LNAME SIZE EVAL 0 � THEN
          'DATA' 'MyVars' 2 �LIST GETVAR PURGE @Limpa variaveis
          "ERRO: Falta variaveis para inequacoes" MSGBOX
          LNAME �STR MSGBOX DROP KILL
          @FAZER O PROGRAMA VOLTAR
        END
        DROP

        @Calcula os valores da inequacoes
        1 MyIqSize FOR Cont
          MyIq Cont 1 �LIST GET @Pega Valor
          EVAL @Calcula valor
        NEXT

        @Duplica resultados
        MyIqSize DUPN

        @Salva na matriz de iteracoes
        MyIqSize �LIST 1 �LIST
        'DATA' 'MyIteracoes' 2 �LIST GETVAR SWAP +
        'DATA' 'MyIteracoes' 2 �LIST SETVAR

        @Verifica se esta OK ou nao
        1 MyIqSize 1 - START AND NEXT
      �

     @Calcula proximo valor da variavel
     MyVarName EVAL
     'DATA' 'MyIncremento' 2 �LIST GETVAR
     +
     'DATA' 'MyValue' 2 �LIST SETVAR

     @Verifica se nao estourou o maximo
     IF DUP 0 == THEN
       IF 'DATA' 'MyValue' 2 �LIST GETVAR 'DATA' 'MyFinal' 2 �LIST GETVAR > 'DATA' 'MyIncremento' 2 �LIST GETVAR 0 > XOR NOT THEN
         DROP 1
         "Solucao nao converge" MSGBOX
       END
     END

    @Reinicia laco caso resposta nao tenha sido encontrada
    UNTIL
    END
  �

  'DATA' 'MyVars' 2 �LIST GETVAR PURGE @Limpa variaveis

  "    GERANDO MATRIZ" 5
  "     DE RESULTADOS" 6
  CLLCD DISP DISP

  @Monta matriz de resultados
  'DATA' 'MyEq' 2 �LIST GETVAR OBJ� �ARRY LNAME SWAP DROP DUP 1 ROW� SWAP
  'DATA' 'MyIq' 2 �LIST GETVAR OBJ� �ARRY 1 ROW�
  SWAP SIZE OBJ� DROP 1 +
  � AddCol
  �
    AddCol COL+

    @Adiciona linhas
    1 'DATA' 'MyIteracoes' 2 �LIST GETVAR SIZE FOR Cont

      @Junta variaveis com inequacoes
      'DATA' 'MyIteracoesV' 2 �LIST GETVAR Cont 1 �LIST GET OBJ� �ARRY 1 ROW�
      'DATA' 'MyIteracoes' 2 �LIST GETVAR Cont 1 �LIST GET OBJ� �ARRY 1 ROW�
      AddCol COL+

      @Arredonda numeros
      3 RND

      @Adiciona a linha
      Cont 1 + ROW+

    NEXT

    @Coloca variavel de iteracao na primeira coluna
    'DATA' 'MyEq' 2 �LIST GETVAR OBJ� �ARRY LNAME SWAP DROP OBJ� DROP
    1 AddCol 1 - FOR Cont
      IF �STR 'DATA' 'MyVarName' 2 �LIST GETVAR �STR == THEN
        AddCol Cont - AddCol Cont - ROLLD
      END
    NEXT
    COL- 1 COL+
  �

  CLLCD

  "Resultados gerados!" MSGBOX
�
'MCilindrica' STO

[
 'C = D/d'                          @Nao muda (Geometrico)
 'K = F / �'                        @Nao muda (Constante de mola)
 'K = (G*d)/(8*C^3*Na)'             @Nao muda (K da mola)
 '�ut = A/(d^m)'                    @Nao muda (Tabela materiais )
 'L0 � (2.63*D)/�'                  @Nao muda (Flambagem - Tab 10.2)
 'L0 � ((1 + �)*�) + ((Na + Ni)*d)' @Nao muda (Restricao fisica)
 '((8*F*D)/(3.14*d^3))*((4*C+2)/(4*C-3)) � 0.45*�ut/ns' @Nao muda (Analise de tensoes)
]
'DATA' 'DefaultCond' 2 �LIST SETVAR

�
  2 2 GETQUERY
  � Material Diametro
  �
    IF Material NOVAL == THEN
      "Selecione o material"
      {
        {}
      }
    END
  �
�
'FioNormaliza' STO


"MOLAS: Instalado" MSGBOX
�
